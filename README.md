# Replace Json in file


A simple utility to quickly replace the contents of text blocks in Json format within JavaScript files.

# Index

*  [Instalation](#instalation)
*  [Basic usage](#basic-usage)
   - [Asynchronous replacement with promises](#asynchronous-replacement-with-promises)
   - [Example Files](#example-files)
   - [Replacement and copy to another file](#replacement-and-copy-to-another-file)
   - [Asynchronous replacement with promises and copy to another file](#asynchronous-replacement-with-promises-and-copy-to-another-file)
* [Return Values](#return-values)
* [License](#license)

## Installation
```shell
# Using npm, installing to local project
npm i --save replace-js-file

# Using npm, installing globally for global cli usage
npm i -g replace-js-file

# Using yarn
yarn add replace-js-file
```

## Basic usage

```js
//Load the library
const replace = require('replace-js-file');
var path = require("path");
var file = path.join(__dirname,'<FILE_PATH>');



var find = {
   "email": {
      "type": "String",
      "unique": true,
      "required": "true"
    },
    "password": "String"
};

var input = {
    "email": {"type":"String","unique":true,"required": true},
    "password": "String",
    "usergroup": {
      "type": "ObjectId",
      "ref": "group",
      "required": true
    }
};

replace(file,find,input,function(err,file_path){
   console.log("path: ",file_path); 
});

```

### Example Files

[Example file](https://gitlab.com/okiloco2/replace-js-file/blob/master/src/models/model1.js)

### Asynchronous replacement with promises

```js

replace(file,find,input)
.then(response => {
    console.log(response.message);
}, err => {
    console.log(err.message);
});

```

### Replacement and copy to another file

```js

var file_output = path.join(__dirname,'<FILE_PATH>');
replace(file,find,input,file_output,function(err,file_path){
   console.log("path: ",file_path); 
});

```

### Asynchronous replacement with promises and copy to another file

```js

var file_output = path.join(__dirname,'<FILE_PATH>');

replace(file,find,input,file_output)
.then(response => {
    console.log(response.message);
}, err => {
    console.log(err.message);
});
    

```

### Return Values

When it is a promise, it returns an object with the answer, in it there are two variables, success and message.

*success* Initiates that everything is successful and *message* details the result of the operation.

When callback is used, two parameters are returned, the first is an *error* and the second the *path* of the created or modified file.

# License
(MIT License)

Copyright 2019, Fabian Vargas, co-founder at [Donec Lab](http://doneclab.com)
