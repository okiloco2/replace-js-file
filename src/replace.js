/**
 * Author: Fabian Vargas Fontalvo
 * Email: f_varga@hotmail.com
 * GitLab: gitlab.com/okiloco2
 * Git:github.com/okiloco
 */
const fs = require("fs");
var beautifyJS = require('js-beautify').js;
const extract = require('extract-json-from-string');
var path = require("path");

/**
 * ReadFile: Read JavaScript Files
 * @param {String} path_file 
 * @param {Function} callback 
 */
function readFile(path_file,callback){
    var data = '';
        fs.createReadStream(path_file, 'utf8')		
    .on('data', function(chunk) {  
        data += chunk;
    })
    .on('error',err => {
        return callback(err);
    })
    .on('end', () => {
        return callback(null,data);
    });
}
/**
 * replaceFile: Replace content into JavaScript File.
 * @param {String} file 
 * @param {String|Object} find 
 * @param {String|Object} input 
 * @param {String} tmp_file 
 */
function replace(file,find,input,tmp_file,callback){

    find = (typeof find === 'object')?JSON.stringify(find):find;
    callback = (typeof callback === 'undefined' && typeof tmp_file === 'function')?tmp_file:callback;

    return new Promise((resolve,reject) => {

        if(typeof input === "undefined"){
            if(typeof callback !== "undefined"){
                return callback({"message":"Missing parameter input."});
            }
            reject({"message":"Missing parameter input.",success:false});
            return;
        }
        if(typeof find === "undefined"){
            if(typeof callback !== "undefined"){
                return callback({"message":"Missing parameter to search."});
            }
            reject({"message":"Missing parameter to search.",success:false});
            return;
        }
        if(typeof file === "undefined"){
            if(typeof callback !== "undefined"){
                return callback({"message":"Missing parameter file."});
            }
            reject({"message":"Missing parameter file.",success:false});
            return;	
        }

        if(typeof tmp_file === "string"){
            if(tmp_file === file){
                if(typeof callback !== "undefined"){
                    return callback({"message":"The output file can not be the same as the input file."});
                }
                reject({"message":"The output file can not be the same as the input file.",success:false});
                return;
            }
        }

        readFile(file,function(err,data) {

            if(err){
                if(typeof callback !== "undefined"){
                    return callback(err);
                }
                reject(err);
                return;
            }

            let objects_json = extract(data)
            .filter(json_obj => {
                var str_json = JSON.stringify(json_obj);
                return (str_json === find);
            });
    
            if(objects_json.length > 0){
                let input_str = (typeof input === 'object')?JSON.stringify(input):input;

                if(find === input_str ){
                    if(typeof tmp_file !== 'string'){
                        if(typeof callback !== "undefined"){
                            return callback(null,(typeof tmp_file === 'string')?tmp_file:file);
                        }
                        resolve({message:"File remains intact.",success:true});
                        return;
                    }
                }
    
                /**
                 * Eliminar saltos, tabs y espacios en blanco en cadena de archivo
                */
                var data_str = data.replace(/[\r\n]/g,'');
                data_str = data_str.replace(/\s\s+/g,'');
                data_str = data_str.replace(/:\s+/g,':');
                data_str = data_str.replace(/{\s+/g,'{');
                data_str = data_str.replace(/\s+}/g,'}');
                data_str = data_str.replace(/,\s+/g,',');
                data_str = data_str.replace(/\s+,/g,',');
                
                data_str = data_str.replace(find,input_str);
                data_str = beautifyJS(data_str, { "indent_size": 2, "space_in_empty_paren": true });
                
                file = (typeof tmp_file === 'string')?tmp_file:file;

                fs.writeFile(file, data_str, 'utf8', function (err) {
                    if(err){
                        if(typeof callback !== "undefined"){
                            return callback(err);
                        }
                        reject({"message":err.message,success:false});
                        return;
                    }
                    if(typeof callback !== "undefined"){
                        return callback(null,(typeof tmp_file === 'string')?tmp_file:file);
                    }
                    resolve({message:(typeof tmp_file === "string")?"File copied successfully":"File updated successfully.",success:true});
                });			
            }else{
                if(typeof callback !== "undefined"){
                    return callback({message:"No schema found in the file."});
                }
                reject({message:"No schema found in the file.",success:false});
            }
        });
    });
}

module.exports = replace;