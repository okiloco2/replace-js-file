const replace = require('./replace');
var path = require("path");

var file = path.join(__dirname,'models/model1.js');
var tmp_file = path.join(__dirname,'dist/model1.js');
var model = {
    "email": {
      "type": "String",
      "unique": true,
      "required": "true"
    },
    "password": "String"
  };
  const UserSchema2 = {
    "email": {
      "type": "String",
      "unique": true,
      "required": true
    },
    "password": "String",
    "usergroup": {
      "type": "ObjectId",
      "ref": "group",
      "required": true
    }
};
var schema = JSON.stringify(model);

var input = {
    "email": {"type":"String","unique":true,"required": "true"},
    "password": "String"
};


replace(file,schema,input,tmp_file)
.then(response => {
    let {success,message} = response;
    if(success){
        console.log(message);
    }else{
        console.log("Atención: ",message);
    }
}, err => {
    console.log(err.message);
});